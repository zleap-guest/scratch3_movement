# scratch3_movement

In scratch add a new sprite.  Within the scripts for this sprite add the following code. 

![4-keys](https://github.com/zleap/scratch3_movement/blob/master/4-keys.png)

This will give you control over sprite movement.  Note the new format of the direction control compared to Scratch 2.0

![move-left](https://github.com/zleap/scratch3_movement/blob/master/move-left.png)

To me this is much better than before and also more user friendly, especially if you are still getting to grips with angles, directions and points of a circle.

You may still need to adjust the properties of the sprite so that it points in the right direction.   However this really depends on the individual sprite.  

Have fun
Created by Paul Sutton - e: zleap@zleap.net 
web: https://personaljournal.ca/paulsutton/

Please feel free to use, modify and contribute to this further.  

![cc-logo](https://github.com/zleap/scratch3_timer/blob/master/88x31.png)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0NDcwMDkzMTRdfQ==
-->
